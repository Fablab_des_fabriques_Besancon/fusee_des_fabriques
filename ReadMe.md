# Fusée des Fabriques
## Modèle de fusée à eau basé sur une bouteille d'eau pétillante.

![](Freecad/Screenshot_20230531_104445.png)

Imaginé à l'occasion d'un atelier, ce modèle de fusée à eau est encore en cours d'amélioration. 

Lanceur "Gardena", raccord inférieur imprimé en PETG, anneau supérieur en PLA. 
Les ailerons sont découpés en polypropylène alvéolaire de 3.5 mm.

Quelques précisions à cette page: https://fablab.grandbesancon.fr/2023/06/07/fusees-a-eau/ 

Développement en cours d'une version avec déploiement de parachute. Documentation plus complète en cours (lanceur, liste de matériel...)

