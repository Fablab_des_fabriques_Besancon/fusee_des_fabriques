/// Based on a Dumphone PCB
/// Select card type Wemos Lolin32

// Remote: press the button to reverse Servo inside the rocket


#include <esp_now.h>
#include <WiFi.h>
#include <esp_wifi.h>

//DISPLAY
#include <soc/rtc.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735
#include <SPI.h>
#include "display.h"

//DISPLAY
// Color definitions
// remember it's uint16_t
#define BLACK    0x0000
#define RED      0x001F
#define BLUE     0xF800
#define GREEN    0x07E0
#define CYAN     0x07FF
#define MAGENTA  0xF81F
#define YELLOW   0xFFE0
#define WHITE    0xFFFF

// Le buffer pour notre écran
aFrameBuffer frame(128, 160);

// MAC Address inside the rocket
uint8_t broadcastAddress[] = {0x24, 0x62, 0xab, 0xd4, 0xcc, 0xdd};

#define btnSelect   39

long lastTime;
// We send the state of the button 10 times in a second
int timerDelay = 100;

// We alwas check the the connection is on
boolean connectionOK = false;

// Must match the sender structure
typedef struct struct_message {
  int buttonValue;
} struct_message;

// Create a struct_message called myData
struct_message myData;

// callback when data is recv from Master
void OnDataRecv(const uint8_t *mac_addr, const uint8_t *data, int data_len) {
  char macStr[18];
  snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
           mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
  Serial.print("Last Packet Recv from: "); Serial.println(macStr);
  Serial.print("Last Packet Recv Data: "); Serial.println(*data);
}

String success;

// Callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
  if (status == 0) {
    success = "Delivery Success :)";
    connectionOK = true;
  }
  else {
    success = "Delivery Fail :(";
    connectionOK = false;
  }
}

esp_now_peer_info_t peerInfo;

void setup() {
  pinMode(btnSelect, INPUT);

  // Init Serial Monitor
  Serial.begin(115200);

  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

// Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);

  peerInfo.channel = 0;
  peerInfo.encrypt = false;
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
      // Add peer
      if (esp_now_add_peer(&peerInfo) != ESP_OK) {
        Serial.println("Failed to add peer");
        return;
      }
}

void loop() {

  // We send the state of the button
  if ((millis() - lastTime) > timerDelay) {
    int value;
    value = digitalRead(btnSelect);
    struct_message mySendedData;
    mySendedData.buttonValue = value;
    esp_now_send(0, (uint8_t *) &mySendedData, sizeof(mySendedData));
    lastTime = millis();
  }

  // We display on screen the state of the connection
  frame.setTextSize(1);
  
  if (connectionOK)
  {
    frame.fillCircle(64, 80, 40, GREEN);
  } else {
    frame.fillCircle(64, 80, 40, RED);
  }
  frame.display(true);

}
