//===============================================================
//  Start code for Micro Demo platform
//  (F) Dzl 2020
//  See more: blog.dzl.dk
//===============================================================

#ifndef __DISPLAY_H
#define __DISPLAY_H

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735
#include <SPI.h>

//Display connections:
#define TFT_CS        26
#define TFT_RST       33 // Or set to -1 and connect to Arduino RESET pin
#define TFT_DC        27


Adafruit_ST7735 tft = Adafruit_ST7735(&SPI, TFT_CS, TFT_DC, TFT_RST);   //-Just used for setup

class  aFrameBuffer : public Adafruit_GFX {
  public:
    uint16_t *buffer;
    aFrameBuffer(int16_t w, int16_t h): Adafruit_GFX(w, h)
    {
      buffer = (uint16_t*)malloc(2 * h * w);
      for (int i = 0; i < h * w; i++)
        buffer[i] = 0;

      tft.initR();
      tft.setRotation(0);
      tft.fillScreen(ST77XX_BLACK);
      
    }
    void drawPixel( int16_t x, int16_t y, uint16_t color)
    {
      if (x > 127)
        return;
      if (x < 0)
        return;
      //        x = 127;
      if (y > 159)
        return;
      if (y < 0)
        return;
      //        y = 159;
      buffer[x + y * _width] = color;
    }

    void display(bool autoClear)
    {
      tft.setAddrWindow(0, 0, 128, 160);
      digitalWrite(TFT_DC, HIGH);
      digitalWrite(TFT_CS, LOW);
      SPI.beginTransaction(SPISettings(80000000, MSBFIRST, SPI_MODE0));

      if (autoClear)
      {
        for (uint16_t i = 0; i < 128 * 160; i++)
        {
          SPI.write16(buffer[i]);
          buffer[i] = 0;
        }
      }
      else
      {
        for (uint16_t i = 0; i < 128 * 160; i++)
          SPI.write16(buffer[i]);
      }
      SPI.endTransaction();
      digitalWrite(TFT_CS, HIGH);
    }
};

//                    bbbb bggg gggr rrrr
//        r             0    0    1    f
//        g             0    7    e    0
//        b             f    8    0    0

uint16_t RGBtoColor(uint8_t r, uint8_t g, uint8_t b)
{

  return (r >> 3) | ((g >> 2) << 5) | ((b >> 3) << 11);
  //return (r>>3) | ((uint16_t)g&0b11111100<<10) | (((uint16_t)b<<15)&0xf8);
}



#endif
